require("dotenv").config();
const Config = require("./config");
const express = require("express");
const morgan = require("morgan");
const app = express();
const cors = require("cors");
const path = require("path");
const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer, {
	pingTimeout: 30000, cors: {
		origins: ["http://localhost:3001", "http://192.168.0.103:3001/", "https://locationapp-frontend.vercel.app/"]
	}
});
const db = require("./db");
const responseTime = require("response-time");
const errorHandler = require("./_utils/error-handler");
const authorization = require("./_utils/authorization");
app.use(authorization());

/*Routes imports*/
const authRoute = require("./api/users");
/*Routes imports*/

app.use("/", express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(responseTime());
app.use(morgan("dev"));
app.use(express.urlencoded({extended: false}));
app.use(cors({origin: (origin, callback) => callback(null, true), credentials: true}));
// enable if wanted to include for all the apis and not in specific methods, // and Add specific route to ignore in ignore list of creating jwt(authorization.js).

app.get("/", function (req, res) {
	res.sendFile(__dirname + "/index.html");
});

io.on("connection", socket => {
	// setInterval(() => {
	// 	socket.emit("SocketAPI", "HI React " + new Date());
	// }, 1000);
	socket.on("send:coords", data => {
		console.log("coords data", data);
	});
});

/*Routes*/
app.use("/api/user", authRoute);
/*Routes*/

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(errorHandler);

app.on("close", () => db.disconnect());
httpServer.listen(Config.PORT, () => {
	console.log(`Server started on port ${Config.PORT}`);
});