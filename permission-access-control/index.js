const role = require("./roles");
const Config = require("./../config");

const AccessControl = require("accesscontrol");

const ac = new AccessControl();

/*() => {
	ac.grant(role?.USER)
		.readOwn("profile")
		.deleteOwn("profile");

	ac.grant(role?.ADMIN)
		.extend(role?.USER)
		.readAny("profile")
		.updateAny("profile")
		.deleteAny("profile");
	return ac;
};*/

const accessControl = (function () {
	ac.grant(role?.USER)
		.readOwn("profile")
		.updateOwn("profile")
		.deleteOwn("profile");

	ac.grant(role?.ADMIN)
		.extend(role?.USER)
		.readAny("profile")
		.updateAny("profile")
		.deleteAny("profile");

	return ac;
})();

const grantAccess = (action, resource) => {
	return async (req, res, next) => {
		if (Config.checkAccessControl) {
			try {
				let permission = accessControl.can(req.user.role)[action](resource);

				if (!permission.granted) {
					return res.status(401).json({error: "Permission denied."});
				}
				next();
			} catch (e) {
				next(e);
			}
		} else {
			next();
		}
	};
};

module.exports = {grantAccess};