const NodeCache = require("node-cache");

class Cache {

  constructor(ttlSeconds = 0) {
    console.log("ttlSeconds", ttlSeconds);
    if (!!Cache.instance) {
      return Cache.instance;
    }
    this.cache = new NodeCache({stdTTL: ttlSeconds, checkperiod: ttlSeconds, useClones: false});
    Cache.instance = this;
    return this;
  }

  getKey(key, storeFunction) {
    const value = this.cache.get(key);
    console.log(`Cache ttl ${key}:`, this.cache.getTtl(key));
    if (value) {
      console.log(`Data returned from cache => ${key} :`, value);
      return Promise.resolve(value);
    }

    return storeFunction().then(result => {
      console.log(`Data returned from DB => ${key} :`, result);
      this.cache.set(key, result);
      return result;
    });
  }

  getTtl(key) {
    return this.cache.getTtl(key);
  }

  getAllKeys() {
    return this.cache.getAllKeys();
  }

  deleteKey(keys) {
    this.cache.del(keys);
  }

  deleteKeyStartsWith(strString = "") {
    if (!strString) {
      return;
    }

    const keys = this.cache.keys();
    keys.forEach(key => {
      if (key.indexOf(strString) === 0) {
        this.del(key);
      }
    });
  }

  flushAllKeys() {
    this.cache.flushAll();
    console.log("Cache flushed", this.cache.getStats());
  }
}

module.exports = Cache;