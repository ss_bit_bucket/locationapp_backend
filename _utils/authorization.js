const expressJwt = require("express-jwt");
const Config = require("../config");
const UserService = require("./../api/users/services");

const authorization = () => {
	const {secret} = Config;
	return [expressJwt({secret, algorithms: ["HS256"], isRevoked}).unless({
		path: [
			"/api/user/authenticate",
			"/api/user/sign-up"
		]
	})];
};

const isRevoked = async (req, payload, done) => {
	const user = await UserService.getById(payload?.sub);
	if (!user) {
		return done(null, true);
	}
	done();
};

module.exports = authorization;