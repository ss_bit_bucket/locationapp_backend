const mongoose = require("mongoose");
const redis = require("redis");
const util = require("util");
const Config = require("./../config");

const client = redis.createClient(Config.REDIS_URL);
client.hget = util.promisify(client.hget);

// create reference for .exec
const exec = mongoose.Query.prototype.exec;

// create new cache function on prototype
mongoose.Query.prototype.cache = function ({key, expire = Config.REDIS_CACHE_EXPIRY}) {
  this.useCache = true;
  this.expire = expire;
  this.hashKey = JSON.stringify("_" + key || this.mongooseCollection?.name);
  return this;
};

// override exec function to first check cache for data
mongoose.Query.prototype.exec = async function () {
  if (!this.useCache) {
    return await exec.apply(this, arguments);
  }

  const key = JSON.stringify({
    ...this.getQuery(),
    collection: this.mongooseCollection?.name
  });

  // get cached value from redis
  const cacheValue = await client.hget(this.hashKey, key);

  // if cache value is not found, fetch data from mongodb and cache it
  if (!cacheValue) {
    const result = await exec.apply(this, arguments);
    client.hset(this.hashKey, key, JSON.stringify(result));
    client.expire(this.hashKey, this.expire);
    console.log("Data returned from Database.");
    return result;
  }

  // return found cachedValue
  const doc = JSON.parse(cacheValue);
  console.log("Data returned from cache.");
  return doc ? Array.isArray(doc)
    ? doc.map(d => new this.model(d))
    : new this.model(doc) : null;
};

function clearCache(hashKey) {
  // client.flushdb(JSON.stringify(hashKey));
  console.log(JSON.stringify(hashKey))
  client.del(JSON.stringify(hashKey), function (err, reply) {
    console.log(reply);
  });
}

module.exports = {clearCache};