const ApiRoutes = {
	authenticate: "/authenticate",
	signUp: "/sign-up",
	users: {
		userById: "/:id",
		users: "/"
	}
};

module.exports = ApiRoutes;