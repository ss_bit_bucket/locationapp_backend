const express = require("express");
const router = express.Router();
const UsersService = require("./services");
const ApiRoutes = require("./../../_utils/api-routes");
const ac = require("./../../permission-access-control");

router.get(ApiRoutes?.users?.users, ac.grantAccess("readAny", "profile"), (req, res, next) => {
	UsersService.getAllUsers()
		.then(data => res.json({data}))
		.catch(err => next(err));
});

router.get(ApiRoutes.users?.userById, ac.grantAccess("readOwn", "profile"), (req, res, next) => {
	UsersService.getById(req?.params?.id)
		.then(data => data ? res.json({data}) :
			res.status(404)
				.json({
					error: {
						code: 404,
						message: "User not found."
					}
				}))
		.catch(err => next(err));
});

router.delete(ApiRoutes?.users?.users, ac.grantAccess("deleteOwn", "profile"), (req, res, next) => {
	UsersService.deleteUser(req?.body)
		.then(data => res.json({data}))
		.catch(err => next(err));
});

router.post(ApiRoutes.authenticate, (req, res, next) => {
	UsersService.authenticate(req?.body)
		.then(data => data ? res.json({data}) :
			res.status(400)
				.json({
					error: {
						code: 404,
						message: "Email or Password incorrect."
					}
				}))
		.catch(err => {
			next(err);
		});
});

router.post(ApiRoutes.signUp, (req, res, next) => {
	UsersService.signUp(req?.body)
		.then(data => res.json({data}))
		.catch(err => next(err));
});

module.exports = router;