const Config = require("../../../config");
const mongoose = require("mongoose");

const UsersSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	phone: {
		type: String,
		unique: true,
		required: true
	},
	role: {
		type: String,
		required: true
	}
}, {
	timestamps: true
});

UsersSchema.set("toJSON", Config?.MONGO_SCHEMA_CONFIG);

module.exports = mongoose.model("user", UsersSchema);