const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Config = require("./../../../config");
const Users = require("./../model/index");

const authenticate = async ({email, password}) => {
	if (!email)
		throw `Email is required.`;
	if (!password)
		throw `Password is required.`;

	const user = await Users.findOne({email});
	if (!user)
		throw "Account does not exists.";

	if (!bcrypt.compareSync(password, user?.password))
		throw "Incorrect Email or password.";

	if (user && user?.id && bcrypt.compareSync(password, user?.password)) {
		const token = jwt.sign({sub: user?.id, role: user?.role}, Config?.secret, {expiresIn: Config?.JWT_EXPIRY});
		if (user?.password) delete user?.password;
		return {
			token,
			...user.toJSON()
		};
	}
};

const signUp = async (params) => {
	if (!params?.email)
		throw `Email is required.`;

	const dbUser = await Users.findOne({phone: params?.phone});

	if (dbUser?.email === params?.email)
		throw `Email '${params?.email}' is already exists.`;

	if (dbUser?.phone === params?.phone)
		throw `User with '${params?.phone}' is already exists.`;

	const user = new Users(params);
	if (params?.password)
		user.password = bcrypt.hashSync(params?.password, 10);

	return await user.save();
};

const getAllUsers = async () => {
	return Users.find();
};

const getById = async id => {
	try {
		return Users.findById(id);
	} catch (e) {
		console.log(e);
	}
	// .cache({key: id});
};

const deleteUser = async ({id}) => {
	if (!id)
		throw `User id is required.`;

	try {
		return Users.findOneAndDelete(id);
	} catch (e) {
		console.log(e);
	}
};

module.exports = {authenticate, deleteUser, getById, getAllUsers, signUp};