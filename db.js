const mongoose = require("mongoose");
const Config = require("./config");

const connectionOptions = {
	useCreateIndex: true,
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
};
console.log(Config.DB_URL_WITH_NAME)
mongoose.connect(Config.DB_URL_WITH_NAME, connectionOptions);
mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;

mongoose.connection.on("connected", () => {
	console.log("DB Connected");
});

mongoose.connection.on("open", () => {
	console.log("DB Opened");
});

mongoose.connection.on("error", console.error.bind(console, "MongoDB connection error:"));

mongoose.connection.on("disconnected", function () {
	console.log("DB disconnected");
});

module.exports = {
	db: mongoose
	// Users: require("./api/users/models")
};