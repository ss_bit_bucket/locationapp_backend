const Config = {
	base: {
		DB_URL: process.env.DB_URL,
		REDIS_URL: "redis://127.0.0.1:6379",
		JWT_EXPIRY: "1d", // 1m = 1 minute, 2d = 1 days
		REDIS_CACHE_EXPIRY: 3600, // 172800 seconds = 2880 mnts = 2 days, Whereas 1440 seconds = 24 mnts,
		// 3600 seconds = 1 hrs = 60 mnts
		MONGO_SCHEMA_CONFIG: {
			virtuals: true,
			versionKey: false,
			transform: (doc, ret) => {
				delete ret._id;
				delete ret.password;
			}
		},
		checkAccessControl: true
	},
	development: {
		secret: process.env.SECRET,
		PORT: process.env.DEVELOPMENT_PORT,
		DB_URL_WITH_NAME: process.env.DB_URL_WITH_NAME
	},
	production: {
		secret: process.env.SECRET,
		PORT: process.env.PRODUCTION_PORT,
		DB_URL_WITH_NAME: process.env.DB_URL_WITH_NAME_LIVE
	}
};

module.exports = (function () {
	return Object.assign(Config.base, Config[process.env.NODE_ENV]);
})();